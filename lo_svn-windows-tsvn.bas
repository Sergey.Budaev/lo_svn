'-------------------------------------------------------------------------------
' LibreOffice macros providing basic integration with Subversion
' This macro uses the TortoiseSVN API
'
' Author Sergey Budaev, sbudaev@gmail.com
'
' References
' More info on LibreOffice Macros: https://www.pitonyak.org/oo.php
'-------------------------------------------------------------------------------

Dim o_svnCommitDialog As Object
Dim svnCurrentDir as String
Dim IsEnvironInit as Boolean
Dim IsAskSaveChanges as Boolean
Dim oDoc As Object
Dim sDocURL as String
Dim sDocName as String
Dim toolDiffodt as String
Dim svnTextCommitMessage as String
Dim svnTextFileStat as String
Dim svnTextLog as String
Dim sTempDir as String
Dim sFileTemp as String 
Dim iLogHistorySize as Integer
Dim iWaitFileComplete as Integer
Dim bDoShowFileStat as Boolean


'-------------------------------------------------------------------------------
' Main entry point, starts the main Dialog
Sub SVN
  svnCommitDialogStart
end Sub


'-------------------------------------------------------------------------------
'                 PARAMETERS ARE DEFINED HERE
'-------------------------------------------------------------------------------
' Initialize the SVN environment, global parameters
Sub svnInitEnviron

  BasicLibraries.LoadLibrary("Tools")

  ' Boolean flag indicating if the Global parameters were ititialized
  IsEnvironInit = True

  ' Define the diff tool for LibreOffice files, normally use `diffodt`
  ' Note that path can use the URL notation, e.g.
  '    `file:///c:/My%20Documents` for `c:\My Documents`
  toolDiffodt = "diffodt"

  ' Define temporary directory, note that must end with valid platform
  ' specific path separator slash ("/" or "\", as defined by `GetPathSeparator()`
  ' Note, we cannot use `Environ("TEMP")` as it can be unreliable,
  ' e.g. Libreoffice started from X or session manager
  sTempDir = Environ("TEMP")
  
  ' Commit message is saved global, is empty at Init
  svnTextCommitMessage = ""

  ' Svn log messages
  svnTextLog = ""

  ' Svn stat output result
  svnTextFileStat = ""

  ' The length of the svn log history, this is the parameter to svn log -l XX
  ' Note that it should not be more than 6-8 to fit on screen because the
  ' MsgBox does not scroll
  iLogHistorySize = 6

  ' Wait duration in ms, for asynchronous file operations.
  ' This is needed to introduce delay to avoid reading incomplete data
  ' while LO or svn command outputs something to a file
  iWaitFileComplete = 400

  ' This is an indicator flag, when TRUE, will show svn stat results dialog
  bDoShowFileStat = True

  ' Wether to reload document silently losing changes at update,
  ' True = ask to save first
  IsAskSaveChanges = True

  ' The names of the temporary files that are used for interfacing with the
  ' svn command
  sFileTemp = sTempDir & "svn_tmp_" & svnRandomStringHex(8)

  ' Get the document URL
  oDoc = ThisComponent
  If oDoc.supportsService( "com.sun.star.chart.ChartDocument" ) Then
    oDoc = oDoc.getParent()
  End If
  If oDoc.HasLocation() Then
    sDocURL = oDoc.getURL()
    sDocName = FileNameoutofPath(sDocURL, "/")
  Else
    MsgBox "The document has not yet been saved", MB_ICONSTOP, "Error"
    Exit sub
  End If

End Sub

'-------------------------------------------------------------------------------
'-------------------------------------------------------------------------------
' Start the SVN commit dialog
Sub svnCommitDialogStart()

  Dim sCommand as String
  Dim o_svnObjectText as Object
  Dim o_svnObjectTURI as Object


  BasicLibraries.LoadLibrary("Tools")
  svnCurrentDir = CurDir

  if ( Not IsEnvironInit ) then
    svnInitEnviron()
  end If

  ' The directory is under version control, show main dialog,
  ' It is simplified on Windows, no URL and revision
  o_svnCommitDialog = LoadDialog("Lo_SVN", "svnCommitDialog")
  o_svnCommitDialog.Execute()

End Sub


'-------------------------------------------------------------------------------
' Get the commit message from the commit dialog
' and execute `svn ci file`
Sub svnCommitDialogDoCommitText ()

  Dim o_svnObjectText as Object

  ' Dim sBuildStr as String

  o_svnObjectText = o_svnCommitDialog.GetControl("svnCommitText")
  svnTextCommitMessage =  o_svnObjectText.Text

  ' Save the document before doing commit
  if ThisComponent.isModified()  then
    If ( ThisComponent.hasLocation() and (Not ThisComponent.isReadOnly()) ) then
      ThisComponent.store()
    end if
  end if

  ' Build and execue the command, note that the
  ' standard output goes to file and the standard error
  ' goes to standard input. So a single file for command
  ' output is produced
  sCommand = "/command:commit /logmsg:" & chr(34) & svnTextCommitMessage & chr(34) & " /path:" & chr(34) & ConvertFromURL(sDocURL) & chr(34)
  Shell ("TortoiseProc.exe", 0, sCommand , True )

  ' Close the main svn dialog after commit
  svnCommitDialogClose()

  ' Force reload  document after commit
  wait iWaitFileComplete
  bSave = IsAskSaveChanges
  IsAskSaveChanges = False
  svnDocumentReload()
  IsAskSaveChanges = bSave

End Sub


'-------------------------------------------------------------------------------
' Close the commit dialog
Sub svnCommitDialogClose()
   If Not IsNull(o_svnCommitDialog) then
     o_svnCommitDialog.endExecute()
   End If
End Sub


'-------------------------------------------------------------------------------
' svn Update the document
Sub svnDoUpdate()

  if ( Not IsEnvironInit ) then
    svnInitEnviron()
  end If

  ' Build the command
  sCommand = "/command:update /path:" & chr(34) & ConvertFromURL(sDocURL) & chr(34)
  Shell ("TortoiseProc.exe", 0, sCommand , False )

  ' Close SVN main dialog
  svnCommitDialogClose()

  ' Reload  document possibly losing all possible unsaved changes
  ' Here reload is not forced asking the user to save unsaved
  ' changes, for  safety
  bSave = IsAskSaveChanges
  IsAskSaveChanges = True
  svnDocumentReload()
  IsAskSaveChanges = bSave

End Sub


'-------------------------------------------------------------------------------
' svn Lock the document
' The primary usage of this function is for documents with this
' property enabled:
'     svn propset 'svn:needs-lock' true document_name.odt
' Then the document will appear read-only after update by default
' and enabling read access is effectively enabled by local locking.
' Lock is disabled on commit then.
Sub svnDoLockDocument()

  Dim bSave as Boolean

  if ( Not IsEnvironInit ) then
    svnInitEnviron()
  end If

  sCommand = "/command:lock /path:" & chr(34) & ConvertFromURL(sDocURL) & chr(34)
  Shell ("TortoiseProc.exe", 0, sCommand , True )

  ' Close SVN main dialog
  svnCommitDialogClose()

  ' Force reload  document
  bSave = IsAskSaveChanges
  IsAskSaveChanges = False
  svnDocumentReload()
  IsAskSaveChanges = bSave

End Sub


'-------------------------------------------------------------------------------
' svn UnLock the document
Sub svnDoUnLockDocument()

  Dim bSave as Boolean

  if ( Not IsEnvironInit ) then
    svnInitEnviron()
  end If

  sCommand = "/command:unlock /path:" & chr(34) & ConvertFromURL(sDocURL) & chr(34)
  Shell ("TortoiseProc.exe", 0, sCommand , True )

  ' Close SVN main dialog
  svnCommitDialogClose()

  ' Force reload  document
  bSave = IsAskSaveChanges
  IsAskSaveChanges = True
  svnDocumentReload()
  IsAskSaveChanges = bSave

End Sub


'-------------------------------------------------------------------------------
' svn Revert the document
Sub svnDoRevertCancelAllChanges()

  Dim bSave as Boolean

  if ( Not IsEnvironInit ) then
    svnInitEnviron()
  end If
  
  
  if ThisComponent.isReadOnly() then
    'ThisComponent.reaload(True)
    sCommand = "/command:revert /path:" & chr(34) & ConvertFromURL(sDocURL) & chr(34)
    Shell ("TortoiseProc.exe", 0, sCommand , True )
  else
    MsgBox "Document must be open 'READ-ONLY' for `revert`:" & chr(13) & "Use Ctrl+Shift+M or toggle Edir->Edit mode", MB_ICONSTOP, "Error"
  end if

  ' Close SVN main dialog
  svnCommitDialogClose()

  ' Reload  document possibly losing all possible unsaved changes
  ' Here reload is not forced asking the user to save unsaved
  ' changes, for  safety
  bSave = IsAskSaveChanges
  IsAskSaveChanges = True
  svnDocumentReload()
  IsAskSaveChanges = bSave

End Sub


'-------------------------------------------------------------------------------
' Do svn stat
Sub svnDoStatFile()

  if ( Not IsEnvironInit ) then
    svnInitEnviron()
  end If

  ' Build and execue the command
  sCommand = "/command:repostatus /path:" & chr(34) & ConvertFromURL(sDocURL) & chr(34)
  Shell ("TortoiseProc.exe", 0, sCommand , False )

End Sub


'-------------------------------------------------------------------------------
' Check the differences the WC and the latest revision
Sub svnCheckDiffWCLatest()

  Dim o_svnObjectText as Object
  Dim svnRevisions as String

  o_svnObjectText = o_svnCommitDialog.GetControl("svnRevDiffRevs")
  svnRevisions =  Replace(o_svnObjectText.Text, ":", " ")
  svnRevisions =  Replace(svnRevisions, "-", " ")

  objExec = CreateUnoService("com.sun.star.system.SystemShellExecute")

  svnInitEnviron()
  sCommand = "/c " & toolDiffodt & " " & svnRevisions & " " & chr(34) & ConvertFromURL(sDocURL) & chr(34)
  objExec.execute("cmd.exe", sCommand, 1)

End Sub

'-------------------------------------------------------------------------------
' Get a copy oif the SVN  log for the file
Sub svnGetLogTxt()

  if ( Not IsEnvironInit ) then
    svnInitEnviron()
  end If

  ' Build and execue the command
  sCommand = "/command:log /path:" & chr(34) & ConvertFromURL(sDocURL) & chr(34)
  Shell ("TortoiseProc.exe", 0, sCommand , False )

End Sub


'-------------------------------------------------------------------------------
' Information pane
Sub svnInformationShow

  Dim iFile as Integer
  Dim sFileStr as String
  Dim sBuildStr as String

  if ( Not IsEnvironInit ) then
    svnInitEnviron()
  end If
  ' Build and execue the command
  sCommand = "/command:properties /path:" & chr(34) & ConvertFromURL(sDocURL) & chr(34)
  Shell ("TortoiseProc.exe", 0, sCommand , False )

End Sub


'-------------------------------------------------------------------------------
' Generate a random text string
function svnRandomStringHex( n as integer )

  Dim TempString as String
  Dim BuildStr as String
  TempString=""
  BuildStr=""
  randomize

  for i = 1 to n
    TempString=Hex(int(rnd*256))
    BuildStr=BuildStr & TempString
  next
  svnRandomStringHex = BuildStr

End Function


'-------------------------------------------------------------------------------
' Reload the document
Sub svnDocumentReload(  )

  if ( Not IsEnvironInit ) then
    svnInitEnviron()
  end If

  ThisComponent.setModified( IsAskSaveChanges )  'Ignore changes if IsAskSaveChanges = False
  document = ThisComponent.CurrentController.Frame
  dispatcher = createUnoService("com.sun.star.frame.DispatchHelper")
  dispatcher.executeDispatch(document, ".uno:Reload", "", 0, Array())

End Sub
