rem DIFF tool for comparing versions of FODT Flat LibreOffice XML files
rem managed by Subversion. Can compare
rem    - working copy with the latest commit (one input parameter);
rem    - working copy with specific revision (two input parameters);
rem    - two arbitrary revisions (three input parameters).
rem
rem See this on using Subversion with LibreOffice files:
rem     https://wiki.documentfoundation.org/Libreoffice_and_subversion
rem
rem Requires: Subversion, diffpdf, LibreOffice
rem
rem Author: Sergey Budaev sergey.budaev@uib.no
IF "%1"=="" GOTO Helpmessage

setlocal

set npars=0
set par1=%1
set par2=%2
set par3=%3

:Loop
if "%1"=="" goto Continue
  set /a npars = %npars% + 1
  shift
goto Loop

:Continue

set LIBREOFFICE_EXEC="C:\Program Files\LibreOffice\program\soffice.exe"
set DIFFPDF="c:\bin\diffpdf.exe"
set WAIT=5

set TMPDIR=%TEMP%
set WORKDIR=%CD%

rem Select which n of parameters was provided
if %npars%==1 call :do_one_param %par1%
if %npars%==2 call :do_two_param %par1% %par2%
if %npars%==3 call :do_three_param %par1% %par2% %par3%

%TMPDIR:~0,2%
cd %TMPDIR%

echo Wait for DIFF: tmp_doc_%REV_FIRST%.pdf tmp_doc_%REV_SECOND%.pdf
%LIBREOFFICE_EXEC% --headless --convert-to pdf tmp_doc_%REV_FIRST%%OOEXT%
%LIBREOFFICE_EXEC% --headless --convert-to pdf tmp_doc_%REV_SECOND%%OOEXT%
%DIFFPDF%  tmp_doc_%REV_FIRST%.pdf tmp_doc_%REV_SECOND%.pdf

call :cleanup

endlocal

exit

rem Short help if no parameters are supplied
:Helpmessage
echo Incorrect argument count
echo Usage: %0 [REV1 REV2] document.fodt
exit 1


rem Get the latest SVN revision for the object defined by the first parameter to %REV%
:get_svn_rev
for /F "tokens=*" %%a in ('svn info --show-item last-changed-revision %1') do set REV=%%a
exit /b 0


rem Runs a command defined by the first parameter and puts the output to %OUT%
rem This is an equivalent to Linux `cmd`
:get_command_output
for /F "tokens=*" %%a in ('%1') do set OUT=%%a
exit /b 0


rem Convert HEAD into the latest revision of the file, otherwise return
rem old revision
:get_head_rev
set REV_IN=%1
set FILE_IN=%2
set HEAD_REV=%REV_IN%
set REV=%REV_IN%
if %REV_IN%==HEAD call :get_svn_rev %FILE_IN%
set HEAD_REV=%REV%
exit /b 0


rem One parameter is provided: fodt file name, compare working copy with the
rem latest revision
:do_one_param
set FILENAME="%1"
set OOEXT=%~x1
call :get_svn_rev %FILENAME%
set REV_FIRST=%REV%
set REV_SECOND=WC
call :get_command_output "svn info --show-item url %FILENAME%"
set SVN_URL=%OUT%
svn export -r %REV_FIRST% --force %SVN_URL% %TMPDIR%\tmp_doc_%REV_FIRST%%OOEXT%
copy %FILENAME% %TMPDIR%\tmp_doc_%REV_SECOND%%OOEXT%
%TMPDIR:~0,2%
cd %TMPDIR%
exit /b 0


rem Two parameters are provided: REV1 FODT, compare working copy with a
rem specific revision
:do_two_param
set REV_FIRST=%1
set FILENAME="%2"
set OOEXT=%~x2
set REV_SECOND=WC
call :get_head_rev %REV_FIRST% %FILENAME%
set REV_FIRST=%HEAD_REV%
call :get_command_output "svn info --show-item url %FILENAME%"
set SVN_URL=%OUT%
svn export -r %REV_FIRST% --force %SVN_URL% %TMPDIR%\tmp_doc_%REV_FIRST%%OOEXT%
copy %FILENAME% %TMPDIR%\tmp_doc_%REV_SECOND%%OOEXT%
%TMPDIR:~0,2%
cd %TMPDIR%
exit /b 0


rem Three parameters are provided: REV1 REV2 FODT, compare two revisions
:do_three_param
set REV_FIRST=%1
set REV_SECOND=%2
set FILENAME="%3"
set OOEXT=%~x3
call :get_head_rev %REV_FIRST% %FILENAME%
set REV_FIRST=%HEAD_REV%
call :get_head_rev %REV_SECOND% %FILENAME%
set REV_SECOND=%HEAD_REV%
call :get_command_output "svn info --show-item url %FILENAME%"
set SVN_URL=%OUT%
svn export -r %REV_FIRST% --force %SVN_URL% %TMPDIR%\tmp_doc_%REV_FIRST%%OOEXT%
svn export -r %REV_SECOND% --force %SVN_URL% %TMPDIR%\tmp_doc_%REV_SECOND%%OOEXT%
%TMPDIR:~0,2%
cd %TMPDIR%
exit /b 0


rem Cleanup
:cleanup
del %TMPDIR%\tmp_doc_%REV_FIRST%%OOEXT%
del %TMPDIR%\tmp_doc_%REV_SECOND%%OOEXT%
del %TMPDIR%\tmp_doc_%REV_FIRST%.pdf
del %TMPDIR%\tmp_doc_%REV_SECOND%.pdf
exit /b 0
