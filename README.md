# README #

Lo_SVN is a simple tool allowing to manage LibreOffice documents with Apache
Subversion. It includes two parts

- `diffodt` script (with minimum external dependencies: bash on Linux and `cmd`
  on Windows) and

- `Lo_SVN` LibreOffice extension.

This extension is also available in the official LibreOffice repository: https://extensions.libreoffice.org/en/extensions/show/4071

**A brief manual PDF is at this link: https://sourceforge.net/projects/lo-svn/files/Lo_SVN.pdf/download .**
