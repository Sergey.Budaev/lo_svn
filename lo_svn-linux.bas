'-------------------------------------------------------------------------------
' LibreOffice macros providing basic integration with Subversion
'
' Author Sergey Budaev, sbudaev@gmail.com
'
' References
' More info on LibreOffice Macros: https://www.pitonyak.org/oo.php
'-------------------------------------------------------------------------------

Dim o_svnCommitDialog As Object
Dim svnCurrentDir as String
Dim IsEnvironInit as Boolean
Dim IsAskSaveChanges as Boolean
Dim oDoc As Object
Dim sDocURL as String
Dim sDocName as String
Dim toolDiffodt as String
Dim svnTextCommitMessage as String
Dim svnTextFileStat as String
Dim svnTextLog as String
Dim sTempDir as String
Dim sFileTemp as String ' also -err
Dim iLogHistorySize as Integer
Dim iWaitFileComplete as Integer
Dim bDoShowFileStat as Boolean


'-------------------------------------------------------------------------------
' Main entry point, starts the main Dialog
Sub SVN
  svnCommitDialogStart
end Sub


'-------------------------------------------------------------------------------
'                 PARAMETERS ARE DEFINED HERE
'-------------------------------------------------------------------------------
' Initialize the SVN environment, global parameters
Sub svnInitEnviron

  BasicLibraries.LoadLibrary("Tools")

  ' Boolean flag indicating if the Global parameters were ititialized
  IsEnvironInit = True

  ' Define the diff tool for LibreOffice files, normally use `diffodt`
  ' Note that path can use the URL notation, e.g.
  '    `file:///c:/My%20Documents` for `c:\My Documents`
  toolDiffodt = "diffodt"

  ' Define temporary directory, note that must end with valid platform
  ' specific path separator slash ("/" or "\", as defined by `GetPathSeparator()`
  ' Note, we cannot use `Environ("TEMP")` as it can be unreliable,
  ' e.g. Libreoffice started from X or session manager
  sTempDir = "/tmp/"

  ' Commit message is saved global, is empty at Init
  svnTextCommitMessage = ""

  ' Svn log messages
  svnTextLog = ""

  ' Svn stat output result
  svnTextFileStat = ""

  ' The length of the svn log history, this is the parameter to svn log -l XX
  ' Note that it should not be more than 6-8 to fit on screen because the
  ' MsgBox does not scroll
  iLogHistorySize = 6

  ' Wait duration in ms, for asynchronous file operations.
  ' This is needed to introduce delay to avoid reading incomplete data
  ' while LO or svn command outputs something to a file
  iWaitFileComplete = 50

  ' This is an indicator flag, when TRUE, will show svn stat results dialog
  bDoShowFileStat = True

  ' Wether to reload document silently losing changes at update,
  ' True = ask to save first
  IsAskSaveChanges = True

  ' The names of the temporary files that are used for interfacing with the
  ' svn command
  sFileTemp = sTempDir & "svn_tmp_" & random_string_hex(8)

  ' Get the document URL
  oDoc = ThisComponent
  If oDoc.supportsService( "com.sun.star.chart.ChartDocument" ) Then
    oDoc = oDoc.getParent()
  End If
  If oDoc.HasLocation() Then
    sDocURL = oDoc.getURL()
    sDocName = FileNameoutofPath(sDocURL, "/")
  Else
    MsgBox "The document has not yet been saved", MB_ICONSTOP, "Error"
    Exit sub
  End If

End Sub

'-------------------------------------------------------------------------------
'-------------------------------------------------------------------------------
' Start the SVN commit dialog
Sub svnCommitDialogStart()

  Dim sCommand as String
  Dim o_svnObjectText as Object
  Dim o_svnObjectTURI as Object


  BasicLibraries.LoadLibrary("Tools")
  svnCurrentDir = CurDir

  if ( Not IsEnvironInit ) then
    svnInitEnviron()
  end If

  ' The directory is under version control, show Commit dialog
  If svnCheckIsUnderVersionControl then
      o_svnCommitDialog = LoadDialog("Lo_SVN", "svnCommitDialog")
      ' Set current tevision number into the dialog
      o_svnObjectText =  o_svnCommitDialog.GetControl("svnTextRevisionNumberShow")
      o_svnObjectText.Text = "Revision " & svnGetDocumentVersion()
      ' Set the repository URL text into the dialog
      o_svnObjectTURI =  o_svnCommitDialog.GetControl("svnTextRepoURLShow")
      o_svnObjectTURI.Text = svnGetDocumentSVNURI()
      ' Finally, execute and show the dialog
      o_svnCommitDialog.Execute()
  ' The File is not under version control, show warning message box
  else
      MsgBox "File " & sDocName & " is not under version control: " & chr(13) & ConvertFromURL(sDocURL) , MB_ICONSTOP, "Error"
  end if

End Sub


'-------------------------------------------------------------------------------
' Get the commit message from the commit dialog
' and execute `svn ci file`
Sub svnCommitDialogDoCommitText ()

  Dim cell_val
  Dim o_svnObjectText as Object
  Dim iFile as Integer
  Dim sFileStr as String
  Dim sBuildStr as String

  o_svnObjectText = o_svnCommitDialog.GetControl("svnCommitText")
  svnTextCommitMessage =  o_svnObjectText.Text

  ' Save the document before doing commit
  if ThisComponent.isModified()  then
    If ( ThisComponent.hasLocation() and (Not ThisComponent.isReadOnly()) ) then
      ThisComponent.store()
    end if
  end if

  ' Build and execue the command, note that the
  ' standard output goes to file and the standard error
  ' goes to standard input. So a single file for command
  ' output is produced
  sCommand = " -c " & Chr(34) & "svn ci --non-interactive -m " & "'" & svnTextCommitMessage & "'" & " " & ConvertFromURL(sDocURL)  & " > " & sFileTemp & " 2>&1" &  Chr(34)
  Shell ("bash", 0, sCommand , True )
  wait iWaitFileComplete

  ' Close the main svn dialog after commit
  svnCommitDialogClose()

  ' Get the info text from the temporary file
  if FileExists ( sFileTemp ) then
    iFile = FreeFile
    sBuildStr = ""
    Open  sFileTemp   for Input as  #iFile
    do while not EOF(iFile)
      Line Input   #iFile,   sFileStr
      sBuildStr = sBuildStr & sFileStr & chr(13)
    Loop
    Close #iFile
  else
    MsgBox "Error in command svn commit ", MB_OK, "Error"
  end if

  ' Force reload  document after commit
  wait iWaitFileComplete
  bSave = IsAskSaveChanges
  IsAskSaveChanges = False
  svnDocumentReload()
  IsAskSaveChanges = bSave

  ' Show svn stat dialog with revision
  bDoShowFileStat = False
  svnDoStatFile()
  MsgBox "File " & sDocName &  ", latest revision " & svnGetDocumentVersion() & Chr(13) &  chr(13) & "COMMIT:" & chr(13) & sBuildStr & chr(13) & chr(13) & "CURRENT STATUS:" & chr(13) & svnTextFileStat & chr(13) & chr(13) & "Tip: If commit failed because the active document is locked (`K`), release the lock, ask the lock holder to unlock or force with `svn unlock --force`. If the commit failed because the document is outdated, use `svn update` command. In case of version conflict (`C`) resolve it or use `svn revert` command loosing local document changes.", MB_OK, "Commit"
  bDoShowFileStat = True

  svnTmpFilesCleanup()

End Sub


'-------------------------------------------------------------------------------
' Close the commit dialog
Sub svnCommitDialogClose()
   If Not IsNull(o_svnCommitDialog) then
     o_svnCommitDialog.endExecute()
   End If
End Sub


'-------------------------------------------------------------------------------
' svn Update the document
Sub svnDoUpdate()

  if ( Not IsEnvironInit ) then
    svnInitEnviron()
  end If

  ' Build the command
  sCommand = " -c " & Chr(34) & "svn up " & ConvertFromURL(sDocURL)   &  Chr(34)
  Shell ("bash", 0, sCommand , True )

  ' Close SVN main dialog
  svnCommitDialogClose()
  svnTmpFilesCleanup()

  bDoShowFileStat = False
  svnDoStatFile()
  MsgBox "Updated " & sDocName &  " to revision " & svnGetDocumentVersion() & Chr(13) & svnTextFileStat, MB_OK, "Update"
  bDoShowFileStat = True

  ' Reload  document possibly losing all possible unsaved changes
  ' Here reload is not forced asking the user to save unsaved
  ' changes, for  safety
  bSave = IsAskSaveChanges
  IsAskSaveChanges = True
  svnDocumentReload()
  IsAskSaveChanges = bSave

End Sub


'-------------------------------------------------------------------------------
' svn Lock the document
' The primary usage of this function is for documents with this
' property enabled:
'     svn propset 'svn:needs-lock' true document_name.odt
' Then the document will appear read-only after update by default
' and enabling read access is effectively enabled by local locking.
' Lock is disabled on commit then.
Sub svnDoLockDocument()

  Dim bSave as Boolean
  Dim iFile as Integer
  Dim sFileStr as String
  Dim sBuildStr as String
  Dim sLockMessages as String
  Dim sLockErrors as String
  Dim sCanEditMsg as String


  if ( Not IsEnvironInit ) then
    svnInitEnviron()
  end If

  sCommand = " -c " & Chr(34) & "svn lock " & ConvertFromURL(sDocURL)  & " > " & sFileTemp & " 2> " & sFileTemp & "-err"  &  Chr(34)
  Shell ("bash", 0, sCommand , True )

  wait iWaitFileComplete / 2

  ' Close SVN main dialog
  svnCommitDialogClose()

  ' Get output messages
  if FileExists ( sFileTemp ) then
    iFile = FreeFile
    sBuildStr = ""
    Open  sFileTemp   for Input as  #iFile
    do while not EOF(iFile)
      Line Input   #iFile,   sFileStr
      if len( trim(sFileStr) ) > 0 then
        sBuildStr = sBuildStr & sFileStr & chr(13)
      end if
    Loop
    Close #iFile
  else
    sBuildStr = "Error in svn lock command 1"
  end if

  sLockMessages = sBuildStr

  ' Get output errors
  if FileExists ( sFileTemp & "-err" ) then
    iFile = FreeFile
    sBuildStr = ""
    Open  sFileTemp & "-err"   for Input as  #iFile
    do while not EOF(iFile)
      Line Input   #iFile,   sFileStr
      if len( trim(sFileStr) ) > 0 then
        sBuildStr = sBuildStr & sFileStr & chr(13)
      end if
    Loop
    Close #iFile
  else
    sBuildStr = "Error in svn lock command 2"
  end if

  sLockErrors = sBuildStr

  if len(trim( sLockErrors) ) = 0 then
    sCanEditMsg = "SUCCESS:" & chr(13) & "You can both edit and commit the document. Note that  the active document will be unlocked after commit. It is advisable to reload or reopen the document to enable write access if there are problems saving (e.g. read-only)."
  else
    sCanEditMsg = "LOCK FAILED:" & chr(13) & "Active Document cannot be commited before it is unlocked (`svn unlock`). Alternatively, force unlock with `svn unlock --force`"
  end If

  MsgBox sLockMessages & Chr(13)  & sLockErrors  & chr(13)  & sCanEditMsg , MB_OK, "Lock document"

  ' Force reload  document
  bSave = IsAskSaveChanges
  IsAskSaveChanges = False
  svnDocumentReload()
  IsAskSaveChanges = bSave

  ' Cleanup temp files
  svnTmpFilesCleanup()

End Sub


'-------------------------------------------------------------------------------
' svn UnLock the document
Sub svnDoUnLockDocument()

  Dim bSave as Boolean
  Dim iFile as Integer
  Dim sFileStr as String
  Dim sBuildStr as String
  Dim sLockMessages as String
  Dim sLockErrors as String
  Dim sOutputMsg as String

  if ( Not IsEnvironInit ) then
    svnInitEnviron()
  end If

  sCommand = " -c " & Chr(34) & "svn unlock " & ConvertFromURL(sDocURL)  & " > " & sFileTemp & " 2> " & sFileTemp & "-err"  &  Chr(34)
  Shell ("bash", 0, sCommand, True )

  ' Close SVN main dialog
  svnCommitDialogClose()

  wait iWaitFileComplete / 2

  ' Get output messages
  if FileExists ( sFileTemp ) then
    iFile = FreeFile
    sBuildStr = ""
    Open  sFileTemp   for Input as  #iFile
    do while not EOF(iFile)
      Line Input   #iFile,   sFileStr
      if len( trim(sFileStr) ) > 0 then
        sBuildStr = sBuildStr & sFileStr & chr(13)
      end if
    Loop
    Close #iFile
  else
    sBuildStr = "Error in svn lock command 1"
  end if

  sLockMessages = sBuildStr

  ' Get output errors
  if FileExists ( sFileTemp & "-err" ) then
    iFile = FreeFile
    sBuildStr = ""
    Open  sFileTemp & "-err"   for Input as  #iFile
    do while not EOF(iFile)
      Line Input   #iFile,   sFileStr
      if len( trim(sFileStr) ) > 0 then
        sBuildStr = sBuildStr & sFileStr & chr(13)
      end if
    Loop
    Close #iFile
  else
    sBuildStr = "Error in svn lock command 2"
  end if

  sLockErrors = sBuildStr

  if len(trim( sLockErrors) ) = 0 then
    sOutputMsg = "SUCCESS: Document unlocked"
  else
    sOutputMsg = "UNLOCK FAILED:" & chr(13) & "This is not error if the above message indicates that the document was not locked. You might also try force unlock with `svn unlock --force`"
  end If

  ' Show svn stat dialog with revision
  bDoShowFileStat = False
  svnDoStatFile()
  MsgBox sLockMessages & Chr(13)  & sLockErrors  & chr(13)  & sOutputMsg & chr(13) & chr(13) & svnTextFileStat, MB_OK, "Unlock document"
  bDoShowFileStat = True

  ' Force reload  document
  bSave = IsAskSaveChanges
  IsAskSaveChanges = True
  svnDocumentReload()
  IsAskSaveChanges = bSave

  ' Cleanup temp files
  svnTmpFilesCleanup()

End Sub


'-------------------------------------------------------------------------------
' svn Revert the document
Sub svnDoRevertCancelAllChanges()

  Dim bSave as Boolean

  if ( Not IsEnvironInit ) then
    svnInitEnviron()
  end If

  sCommand = " -c " & Chr(34) & "svn revert " & ConvertFromURL(sDocURL)   &  Chr(34)
  Shell ("bash", 0, sCommand, True )

  ' Need to wait for the async revert command to complete,
  ' then the following svn stat gives correct result
  wait iWaitFileComplete

  bDoShowFileStat = False
  svnDoStatFile()
  MsgBox "Reverted " & sDocName &  " to revision " & svnGetDocumentVersion() & Chr(13) & svnTextFileStat & chr(13) & chr(13) & "It is advisable to Reload the document", MB_OK, "Revert"
  bDoShowFileStat = True

  ' Close SVN main dialog
  svnCommitDialogClose()
  svnTmpFilesCleanup()

  ' Reload  document possibly losing all possible unsaved changes
  ' Here reload is not forced asking the user to save unsaved
  ' changes, for  safety
  bSave = IsAskSaveChanges
  IsAskSaveChanges = True
  svnDocumentReload()
  IsAskSaveChanges = bSave

End Sub


'-------------------------------------------------------------------------------
' Do svn stat
Sub svnDoStatFile()

  Dim iFile as Integer
  Dim sFileStr as String
  Dim sBuildStr as String

  if ( Not IsEnvironInit ) then
    svnInitEnviron()
  end If

  ' Build and execue the command
  sCommand = " -c " & Chr(34) & "svn stat  " &  ConvertFromURL(sDocURL) & " > " & sFileTemp  &  Chr(34)
  Shell ("bash", 0, sCommand, True )
  wait iWaitFileComplete/2

  ' Get the file lines from the temporary file and build the whole text
  if FileExists ( sFileTemp ) then
    iFile = FreeFile
    sBuildStr = ""
    Open  sFileTemp   for Input as  #iFile
    do while not EOF(iFile)
      Line Input   #iFile,   sFileStr
      sBuildStr = sBuildStr & sFileStr & chr(13)
    Loop
    Close #iFile
  else
    sBuildStr = "Error in svn stat command"
  end if

  svnTextFileStat = sBuildStr

  if len(svnTextFileStat) < 1 then
    svnTextFileStat = "No changes at revision " & svnGetDocumentVersion
  end if

  ' Show the message box whan called from Dialog button, but not show
  ' if called from svn update button
  if bDoShowFileStat then
    MsgBox svnTextFileStat, MB_OK, "Status"
  end if

  if FileExists ( sFileTemp ) then
    Kill  sFileTemp
  end if

End Sub


'-------------------------------------------------------------------------------
' Check the differences the WC and the latest revision
Sub svnCheckDiffWCLatest()

  Dim o_svnObjectText as Object
  Dim svnRevisions as String

  o_svnObjectText = o_svnCommitDialog.GetControl("svnRevDiffRevs")
  svnRevisions =  Replace(o_svnObjectText.Text, ":", " ")
  svnRevisions =  Replace(svnRevisions, "-", " ")

  svnInitEnviron()
  Shell ( toolDiffodt & " " &  svnRevisions & " " & ConvertFromURL(sDocURL)  )
End Sub

'-------------------------------------------------------------------------------
' Get a copy oif the SVN  log for the file
Sub svnGetLogTxt()

  Dim iFile as Integer
  Dim sFileStr as String
  Dim sBuildLogStr as String

  if ( Not IsEnvironInit ) then
    svnInitEnviron()
  end If

  ' Build and execue the command
  sCommand = " -c " & Chr(34) & "svn log -l " & str(iLogHistorySize) & " " & ConvertFromURL(sDocURL) & " > " & sFileTemp  &  Chr(34)
  Shell ("bash", 0, sCommand, True )
  wait iWaitFileComplete

  ' Get the log lines from the temporary file and build the whole text
  if FileExists ( sFileTemp ) then
    iFile = FreeFile
    sBuildLogStr = ""
    Open  sFileTemp   for Input as  #iFile
    do while not EOF(iFile)
      Line Input   #iFile,   sFileStr
      sBuildLogStr = sBuildLogStr & sFileStr & chr(13)
    Loop
    Close #iFile
  else
    svnTextLog = "Error in svn log command"
  end if

  svnTextLog = sBuildLogStr

  MsgBox svnTextLog, MB_OK, "Revision log for " & sDocName

  if FileExists ( sFileTemp ) then
    Kill  sFileTemp
  end if

End Sub

'-------------------------------------------------------------------------------
' Get the SVN revision number of the current document
Function svnGetDocumentVersion()

  Dim iFile as Integer
  Dim sFileStr as String
  Dim iRevision as Integer

  if ( Not IsEnvironInit ) then
    svnInitEnviron()
  end If

  iRevision = -9999

  ' Build and execue the command, does not seem to work with sh -c
  sCommand = " -c " & Chr(34) & "svn info --show-item last-changed-revision " & ConvertFromURL(sDocURL) & " > " & sFileTemp  &  Chr(34)
  Shell ("bash", 0, sCommand, True )
  wait iWaitFileComplete/2

  ' Get the revision mumber from the temporary file
  if FileExists ( sFileTemp ) then
    iFile = FreeFile
    Open  sFileTemp   for Input as  #iFile
    do while not EOF(iFile)
      Line Input   #iFile,  sFileStr
      iRevision = cint(sFileStr)
    Loop
    Close #iFile
  else
    svnGetDocumentVersion = iRevision
    MsgBox "Cannot determine revision ", MB_OK, "Error"
  end if

  ' Final check, revision number is positive nonzero
  if iRevision > -1 then
    svnGetDocumentVersion = iRevision
  end if

  if FileExists ( sFileTemp ) then
    Kill  sFileTemp
  end if

End Function


'-------------------------------------------------------------------------------
' Get the SVN URI of the current document
Function svnGetDocumentSVNURI() as String

  Dim iFile as Integer
  Dim sRepoURL as String

  if ( Not IsEnvironInit ) then
    svnInitEnviron()
  end If

  sRepoURL = "unknown"

  ' Build and execue the command, does not seem to work with sh -c
  sCommand = " -c " & Chr(34) & "svn info --show-item url " & ConvertFromURL(sDocURL) & " > " & sFileTemp  &  Chr(34)
  Shell ("bash", 0, sCommand, True )
  wait iWaitFileComplete/2

  ' Get the revision mumber from the temporary file
  if FileExists ( sFileTemp ) then
    iFile = FreeFile
    Open  sFileTemp   for Input as  #iFile
    do while not EOF(iFile)
      Line Input   #iFile,   sRepoURL
    Loop
    Close #iFile
  else
    svnGetDocumentSVNURI = sRepoURL
    MsgBox "Cannot determine repository URL ", MB_OK, "Error"
  end if

  svnGetDocumentSVNURI = sRepoURL

  if FileExists ( sFileTemp ) then
    Kill  sFileTemp
  end if

End Function


'-------------------------------------------------------------------------------
' Check if the document/ current directory is under version control
Function svnCheckIsUnderVersionControl()
  if svnGetDocumentVersion() > 0 then
    svnCheckIsUnderVersionControl = True
  else
    svnCheckIsUnderVersionControl = False
  end if
End Function

'-------------------------------------------------------------------------------
' Delete temporary files
Sub svnTmpFilesCleanup

  if FileExists ( sFileTemp ) then
    Kill  sFileTemp
  end if

  if FileExists ( sFileTemp & "-err" ) then
    Kill  sFileTemp & "-err"
  end if

End Sub

'-------------------------------------------------------------------------------
' Information pane
Sub svnInformationShow

  Dim iFile as Integer
  Dim sFileStr as String
  Dim sBuildStr as String

  if ( Not IsEnvironInit ) then
    svnInitEnviron()
  end If

  ' First, svn properties are listed, this is especially useful to check for 'svn:needs-lock',
  ' Build and execue the command, does not seem to work with sh -c
  sCommand = " -c " & Chr(34) & "svn proplist  " &  ConvertFromURL(sDocURL) & " > " & sFileTemp  &  Chr(34)
  Shell ("bash", 0, sCommand, True )
  wait iWaitFileComplete / 2

  ' Second, a separator is placed into the same file, manually with sequential file
  iFile = FreeFile
  Open  sFileTemp   for Append  as #iFile
  Print #iFile, "============================================================"
  Print #iFile, "Document Information (svn info):"
  Print #iFile, "        "
  Close #iFile

  ' Third, svn info command is built and appended with >> to the same output file
  sCommand = " -c " & Chr(34) & "svn info '" & ConvertFromURL(sDocURL) & "' >> " & sFileTemp  &  Chr(34)
  Shell ("bash", 0, sCommand, True )
  wait iWaitFileComplete

  ' Get the info text from the temporary file
  if FileExists ( sFileTemp ) then
    iFile = FreeFile
    sBuildStr = "Document Subversion information:" & chr(13) & chr(13)
    Open  sFileTemp   for Input as  #iFile
    do while not EOF(iFile)
      Line Input   #iFile,   sFileStr
      ' Note Null strings in the file cause EOF errors
      if len(sFileStr)<2 then
        exit do
      end if
      sBuildStr = sBuildStr & sFileStr & chr(13)
    Loop
    Close #iFile
  else
    MsgBox "Error in command svn info ", MB_OK, "Error"
  end if

  MsgBox sBuildStr, MB_OK, "Document Information"

  if FileExists ( sFileTemp ) then
    Kill  sFileTemp
  end if

End Sub


'-------------------------------------------------------------------------------
' Generate a random text string
function random_string_hex( n as integer )
Dim TempString as String
Dim BuildStr as String
TempString=""
BuildStr=""
randomize

for i = 1 to n
  TempString=Hex(int(rnd*256))
  BuildStr=BuildStr & TempString
next
random_string_hex = BuildStr

End Function


'-------------------------------------------------------------------------------
' Reload the document
Sub svnDocumentReload(  )

  if ( Not IsEnvironInit ) then
    svnInitEnviron()
  end If

  ThisComponent.setModified( IsAskSaveChanges )  'Ignore changes if IsAskSaveChanges = False
  document = ThisComponent.CurrentController.Frame
  dispatcher = createUnoService("com.sun.star.frame.DispatchHelper")
  dispatcher.executeDispatch(document, ".uno:Reload", "", 0, Array())

End Sub
